tutorial_sprites={}
tutorial_sprites_animated={}
tutorial_sprites_movable={}
tutorial_screen=1

function draw_tutorial()
    -- positions are absolute screen positions, not cell or map position
    if (not state.tutorial_enabled) return
    if btnp(0) or btnp(1) then
        -- triggers recreation of sprites in manage_tutorial_sprites
        clean_up_tutorial_sprites()
        -- sets target tutorial
        tutorial_screen = getnext(tutorial_screen, tutorial_screens, btnp(0))
        return
    end
    for i, v in pairs(tutorial_data[tutorial_screen].text) do
        print(v[1],v[2],v[3])
    end
    print(tutorial_screen, 12, 112)
    manage_tutorial_sprites()
    foreach(tutorial_sprites_animated,animate_stationary)
    foreach(tutorial_sprites_movable,move_actor)
    foreach(tutorial_sprites,draw_actor)
end

function manage_tutorial_sprites()
    if is_empty(tutorial_sprites) then
        for i, v in pairs(tutorial_data[tutorial_screen].sprites) do
            if v[4] == 'belt' then
                sprite = actor:new(v[1], v[2] + 0.5, v[3] + 0.5, 'belt')
                sprite.frames = 4
                sprite.stationary = true
                if (v[5] == 'down') sprite.direction_y = -1
                if (v[5] == 'left') sprite.direction_x = -1
                add(tutorial_sprites_animated,sprite)
                state.occupied_belt[sprite.x - 0.5 .. ',' .. sprite.y - 0.5] = sprite
            elseif v[4] == 'building' then
                sprite = actor:new(v[1], v[2] + 0.5, v[3] + 0.5, 'building')
                add(tutorial_sprites_animated, sprite)
            elseif v[4] == 'movable' then
                sprite = actor:new(v[1], v[2] + 0.5, v[3] + 0.5, 'movable')
                sprite.frames = 1
                add(tutorial_sprites_movable,sprite)
            else
                sprite = actor:new(v[1], v[2] + 0.5, v[3] + 0.5, 'tutorial')
            end
            add(tutorial_sprites,sprite)
        end
    end
    make_buttons_pretty()
end

function make_buttons_pretty()
    for i,v in pairs(tutorial_sprites) do
        if v.sprite_id==177 or v.sprite_id==181 then
            if (btn(0)) v.sprite_id=181 else v.sprite_id=177
        elseif v.sprite_id==178 or v.sprite_id==182 then
            if (btn(1)) v.sprite_id=182 else v.sprite_id=178
        elseif v.sprite_id==179 or v.sprite_id==183 then
            if (btn(2)) v.sprite_id=183 else v.sprite_id=179
        elseif v.sprite_id==180 or v.sprite_id==184 then
            if (btn(3)) v.sprite_id=184 else v.sprite_id=180
        elseif v.sprite_id==161 or v.sprite_id==185 then
            if (btn(4)) v.sprite_id=185 else v.sprite_id=161
        elseif v.sprite_id==162 or v.sprite_id==186 then
            if (btn(5)) v.sprite_id=186 else v.sprite_id=162
        end
    end
end

function clean_up_tutorial_sprites()
    tutorial_sprites = {}
    if not is_empty(tutorial_sprites_animated) then
        for i, v in pairs(tutorial_sprites_animated) do
            state.occupied_belt[v.x - 0.5 .. ',' .. v.y - 0.5] = nil
        end
    end
    tutorial_sprites_animated = {}
    tutorial_sprites_movable = {}
end
