globals={
    MAP_WIDTH=16,
    MAP_HEIGHT=16,
    -- grid cell unit size
    cell_size=8,
    max_frames=60,
    max_products=200,
    structure_map_x=16,
    player_sprite_id=64,
}
