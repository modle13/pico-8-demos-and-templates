function get_wave_cells()
    state.wave_cells=get_map_cells('wave',wave_sprite_ids)
    if (state.reference_wave==nil) state.reference_wave=state.wave_cells[1]
end

function get_map_cells(cell_name,filter_ids)
    local map_cells={}
    local solid_cells={}
    for y=0,15 do
        for x=0,15 do
            sprite_id=mget(x,y)
            if contains(filter_ids,sprite_id) then
                found_sprite={name=cell_name,x=x,y=y,frames=2,sprite_id=sprite_id,frame=1}
                add(map_cells,found_sprite)
            end
            if fget(sprite_id,1) then
                solid_cells[prpos(x+0.5,y+0.5)]=true
            end
        end
    end
    state.solid_map_sprites[prpos(state.room_x,state.room_y)]=solid_cells
    return map_cells
end

function update_map_cells(cell,frame)
    if (cell.name=='wave') frame=state.reference_wave.frame
    mset(getvec(cell),cell.sprite_id+frame)
end

function offset_room(a) return a.x+state.room_x*globals.MAP_WIDTH,a.y+state.room_y*globals.MAP_HEIGHT end

function update_room(axis,value)
    if axis=='x' then
        set_room(state.room_x+value,state.room_y)
    elseif axis=='y' then
        set_room(state.room_x,state.room_y+value)
    end
end

function set_room(x,y)
    -- only trigger on room change
    if (state.room_x==x and state.room_y==y) return false
    printh('room changed to '..prpos(x,y))
    old_x=state.room_x
    old_y=state.room_y
    state.room_x=x
    state.room_y=y
    local new_map_sprites=map_sprites[prpos(x,y)]
    if new_map_sprites==nil then
        printh('did not find sprite map for room '..prpos(x,y))
        -- reset
        state.room_x=old_x
        state.room_y=old_y
        return false
    end
    -- set current room lookup key
    state.room=prpos(state.room_x,state.room_y)
    local count=0
    for sprite in all(split(new_map_sprites)) do
        if sprite~='' then
            local new_x=count%16
            local new_y=flr(count/16)
            mset(new_x,new_y,sprite)
            count+=1
        end
    end
    set_visible_objects()
    get_wave_cells()
    return true
end

function set_visible_objects()
    -- clear visible map
    state.visible_sprites={}
    state.room_map_sprites={}
    for x=0,15 do
        for y=0,15 do
            mset(x+globals.structure_map_x,y,0)
        end
    end

    -- set current room map structures
    for sprite in all(state.structures) do
        if sprite.room==state.room then
            mset(sprite.x+globals.structure_map_x,sprite.y,sprite.sprite_id)
            add(state.room_map_sprites,sprite)
        end
    end
    -- update visible sprites table
    for sprite in all(state.products) do
        if sprite.room==state.room then
            add(state.visible_sprites,sprite)
        end
    end
end

function export_map_sprites()
    -- this will export the 2nd vertical 16x16 section of the map sheet as a string
    -- of comma-separated sprite-ids with left-right top-bottom sequencing
    cells_repr=''
    -- for design map
    -- loop through each x of y to get left-right top-bottom sequencing
    for y=16,31 do
        for x=0,15 do
            -- get current cell sprite id
            sprite_id=mget(x, y)
            cells_repr=cells_repr .. sprite_id .. ','
        end
    end
    printh(cells_repr)
end

function clear_structure_map_cell(x,y) mset(x+globals.structure_map_x,y,0) end

function find_structure(x,y)
    for sprite in all(state.room_map_sprites) do
        if (sprite.rel_x==x and sprite.rel_y==y) return sprite
    end
end
