function handle_coordinates_debug()
    if (not state.debug) return
    if (not state.show_coords) return

    local cursor_pos_text=flr(cursor1.x)..','..flr(cursor1.y)
    print(cursor_pos_text,cursor1.x*8-8,cursor1.y*8-8)

    print('MEM',9.5*globals.cell_size,9)
    print(stat(0),12*globals.cell_size,9)

    print('CPU TOT',7.5*globals.cell_size,17)
    print(stat(1),12*globals.cell_size,17)

    print('CPU SYS',7.5*globals.cell_size,25)
    print(stat(2),12*globals.cell_size,25)
end
