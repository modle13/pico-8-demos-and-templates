cursor={}

function cursor:new(cursor_type)
    local o=actor:new(0,6,6,cursor_type)
    setmetatable(o,self)
    self.__index=self
    if cursor_type=='arrow' then
        o.sprite_id=cursors.arrow[1]
    elseif cursor_type=='building' then
        o.sprite_id=cursors.building[1]
    elseif cursor_type=='delete' then
        o.sprite_id=cursors.delete[1]
    end
    return o
end

function cursor:handle_target()
    target_sprite = check_for_structure(getvec(self))
    if target_sprite~='' then --found thing
        if self.name=='arrow' and target_sprite=='belt' then
            orient_belt(getvec(self))
            sfx(38)
        elseif self.name=='building' and target_sprite=='building' then
            convert_building(getvec(self))
            sfx(38)
        elseif self.name=='delete' then
            if target_sprite~='' and contains(structure_names,target_sprite) then
                mset(self.x+globals.structure_map_x,self.y,0)
                self:delete_target(self.x,self.y,target_sprite)
                sfx(37)
            end
        end
    else --place it
        current_x,current_y=convert_to_pixels(getvec(self))
        if (solid(getvec(self))) sfx(34) return
        if self.name == 'arrow' then
            make_belt(self.x,self.y,{['direction']=self.direction})
            sfx(36)
        elseif self.name=='building' then
            make_building(self.sprite_id,self.x+0.5,self.y+0.5)
            sfx(36)
        end
    end
end

function cursor:delete_target(x,y,sprite_type)
    for target in all(state.room_map_sprites) do
        local check_x=x
        local check_y=y
        if (sprite_type=='building') check_x+=0.5 check_y+=0.5
        if target.rel_x==check_x and target.rel_y==check_y then
            del(state.production_structures,target)
            del(state.structures,target)
            del(state.room_map_sprites,target)
            state.occupied_belt[prpos(x,y)]=nil
            clear_structure_map_cell(x,y)
            break
        end
    end
end

function cursor:change_to_selector(target)
    self.sprite_id=tonum(target.sprite_id)
    self.label=target.label
    self.direction=target.direction
    self.name=target.name
end

function cursor:control()
    if btn(5) then
        -- then this is sprite selection mode; don't move controllable sprite
        return
    end
    -- restrict how often the cursor can move
    if state.reference_frame % 4 != 0 then
        return
    end
    if (btn(0)) self.x += -1
    if (btn(1)) self.x +=  1
    if (btn(2)) self.y += -1
    if (btn(3)) self.y +=  1
    -- prevent moving cursor off screen in x direction
    if (self.x > 15) self.x = 15
    if (self.x <  0) self.x = 0
    -- prevent moving cursor off screen in y direction
    if (self.y > 15) self.y = 15
    if (self.y <  0) self.y = 0
end

function create_cursors()
    arrow_curs=cursor:new('arrow')
    delete_curs=cursor:new('delete')
    building_curs=cursor:new('building')
    default_curs=cursor:new('arrow')
    cursor1=cursor:new('arrow')
end

function cycle_cursor_type(up)
    local target = nil
    local next_type = getnext(cursor1.name,cursor_sprites,up)
    printh('cursor name is '..cursor1.name)
    printh('next type is '..next_type)
    if (next_type=='arrow') target=arrow_curs
    if (next_type=='delete') target=delete_curs
    if (next_type=='building') target=building_curs
    if (target == nil) return
    cursor1:change_to_selector(target)
end

function cycle_selection_sprite(name,left)
    local cspr=cursor1.sprite_id
    cursor1.sprite_id=getnext(cspr,cursors[name],left)
    if cursor1.name=='building' then
        cursor1.label=production_map[cspr].name
    elseif cursor1.name=='arrow' then
        cursor1.direction=arrow_directions[cursor1.sprite_id]
    end
end
