--you have to burn the rope
--BASED ON A mAZAPAN GAME

state={}
-->8
--functions

function limit(n,mx)
    return mid(-mx,n,mx)
end

function round(n)
    return flr(n+0.5)
end

function roll_dice()
    --global!
    dice=rnd()
end

function ramp(tbl,prog,full)
    return tbl[mid(1,round(prog/full*#tbl)+1,#tbl)]
end

function left_pad(s,l,c)
    c=c or " "
    s=tostr(s)
    while #s<l do
        s=c..s
    end
    return s
end

function expand(s)
    s=s..","
    tbl={}
    local j=1
    for i=1,#s do
        if sub(s,i,i)=="," then
            add(tbl,tonum(sub(s,j,i-1)))
            j=i+1
        end
    end
    return tbl
end

function ssget(x,l)
    l=l or 1
    if l==1 then
        return sget(x&127,x>>>7)
    else
        local s=0
        for i=0,l-1 do
            s=s*16+ssget(x+i)
        end
        return s
    end
end

function cprint(s,x,y,c,d)
    if (not c) c=color()
    color(c)
   
    local w=#s-1
    for i=1,#s do
        w+=(sub(s,i,i)>="\x80" and 7 or 3)
    end
   
    if not d then
        print(s,x-w/2,y,c)
    else
        bprint(s,x-w/2,y,c,d)
    end
end

function bprint(s,x,y,c,d)
    for a=x-1,x+1 do
        for b=y-1,y+1 do
            print(s,a,b,d)
        end
    end
    print(s,x,y,c)
end

function init_tprint()
    local font,j="10011730c0337d5f337eb34889366aa103222e21d1355d5311c4211020841103088833e3e343f234ab932eb13754c326b7326ae30cb932eaa33aa210a21502144214a208a20f535a4c23da23df225e23fe22de215e23d63785f11d23b03499f11f378de3705e37a5e21de23ce205e2356225f37a0e33b0e37b1e3499233b1622da347ff3208237ff13082222102041378be32abf223f33a3f22bf20bf3762e37c9f347f121f136c9f221f37cdf3783f33a2e308bf37b2e368bf21b6307e137e0f33f0f37d9f36c9b31f87227d345c411b311d13046237fff51ffffff5155555550fd7b4f50ecc72e51502815502f39e850eeffee5077f9e7504745c4504fb7e4504f7fc450e8c76e51fadebf5010ff1850e8d62e50477dc45040100450edc62e504e3b84511dff7150e9c66e508202085041110450eaeeae515ad6b551f07c1f",1
    tprint_bmp,tprint_w={},{}
    for i=0,121 do
        local w=tonum(sub(font,j,j))
        local bmp="00"..sub(font,j+1,j+ceil(w*1.25))
        tprint_w[i+32]=w
        tprint_bmp[i+32]=tonum("0x"..sub(bmp,-7,-5).."."..sub(bmp,-4))
        j+=ceil(w*1.25)+1
    end
end

function tprint(s,x,y,c,d)
    if (not c) c=color()
    color(c)
    local camx,camy=peek2(0x5f28),peek2(0x5f2a)
    if (x-camx<-128 or x-camx>128) return
   
    local xx,yy=x,y-1
    for i=1,#s do
        local ch=sub(s,i,i)
        if ch=="\n" then
            yy+=6
            xx=x
        else
            local cs=tprint_bmp[ord(ch)] or 0x0.7fff
            repeat
                if xx-camx>=0 and xx-camx<=127 then
                    for rw=1,5 do
                        if cs>>>rw-1&0x0.0001>0 then
                            if (d) rectfill(xx,yy+rw,xx+1,yy+rw+1,d)
                            pset(xx,yy+rw,c)
                        end
                    end
                end
                cs>>>=5
                xx+=1
            until cs==0
            xx+=1
        end
    end
end

function tprint_width(s)
    local w=#s-1
    for i=1,#s do
        w+=tprint_w[ord(sub(s,i,i))] or 3
    end
    return w
end

function ctprint(s,x,y,c,d)
    tprint(s,x-tprint_width(s)/2,y,c,d)
end

function btnf(b,p)
    p=(p or 0)*8
    return btns&1<<(b+p)>0
    and oldbtns&1<<(b+p)==0
end

function btnh(b,p)
    p=(p or 0)*8
    return btns&1<<(b+p)>0
end

function btnr(b,p)
    p=(p or 0)*8
    return btns&1<<(b+p)==0
    and oldbtns&1<<(b+p)>0
end


function collide_map(obj,aim,flag)
    local x,y,w,h=obj.x+obj.cx,obj.y+obj.cy,obj.cw,obj.ch
    local x1,x2,y1,y2
   
    --set bounding boxes
    if aim==⬅️ then
        x1=x-1  x2=x
        y1=y  y2=y+h-1
    elseif aim==➡️ then
        x1=x+w  x2=x+w+1
        y1=y  y2=y+h-1
    elseif aim==⬆️ then
        x1=x  x2=x+w-1
        y1=y-1  y2=y
    elseif aim==⬇️ then
        x1=x  x2=x+w-1
        y1=y+h  y2=y+h+1
    else
        x1=x  x2=x+w-1
        y1=y  y2=y+h-1
    end
   
    --pixels to tiles
    x1\=8 x2\=8
    y1\=8 y2\=8
   
    --loop over tiles
    --to check for flag
    for xx=x1,x2 do
        for yy=y1,y2 do
            if fget(mget(xx&127,yy+((xx&-128)>>2)),flag) then
                return true
            end
        end
    end
   
    return false
end

function collide_sprites(sp1,sp2)
    local sp1x,sp1y,sp2x,sp2y=sp1.x+sp1.cx,sp1.y+sp1.cy,sp2.x+sp2.cx,sp2.y+sp2.cy
   
    return sp1x<sp2x+sp2.cw
    and sp1x+sp1.cw>sp2x
    and sp1y<sp2y+sp2.ch
    and sp1y+sp1.ch>sp2y
end

function collide_sprite_circle(sp,cr)
    local sx,sy,x,y=sp.x+sp.cx,sp.y+sp.cy,cr.x,cr.y
    if cr.x<sx then
        x=sx
    elseif cr.x>sx+sp.cw then
        x=sx+sp.cw
    end
    if cr.y<sy then
        y=sy
    elseif cr.y>sy+sp.ch then
        y=sy+sp.ch
    end
   
    local dx,dy=cr.x-x,cr.y-y
    dx/=16  dy/=16
    return sqrt(dx*dx+dy*dy)<=cr.r/256
end


function destroy(self,objlist)
    del(objlist,self)
end

function format_timer(t)
    return left_pad(t.m,2,"0")..":"..
    left_pad(t.s,2,"0").."."..
    left_pad(t.f*100\30,2,"0")
end

local curr_state
function switch_state(state,...)
 local prev=curr_state
 curr_state=state
 if curr_state.enter then
  curr_state:enter(prev,...)
 end
end
-->8
--init/update/draw

function _init()
    oldbtns,btns=0,0
    init_tprint()
    switch_state(state.title)
end

function _update()
    oldbtns,btns=btns,btn()
    if curr_state.update then
        curr_state:update()
    end
end

function _draw()
    if curr_state.draw then
        curr_state:draw()
    end
    fade_pal()
end

fade=0

fade_tbl={}
for i=1,16 do
    add(fade_tbl,expand(({
        "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0",
        "1,1,129,129,129,129,129,129,129,129,0,0,0,0,0",
        "7,7,7,7,7,7,7,7,7,7,7,7,7,7,7",
        "130,130,130,128,128,128,128,128,128,128,128,0,0,0,0",
        "8,8,136,136,136,136,132,132,132,130,128,128,128,128,0",
        "9,9,9,4,4,4,4,132,132,132,128,128,128,128,0",
        "10,10,138,138,138,4,4,4,132,132,133,128,128,128,0",
        "7,6,6,6,134,134,134,134,5,5,5,133,130,128,0",
        "132,132,132,130,130,130,128,128,128,128,128,128,0,0,0",
        "6,6,134,13,13,13,141,5,5,5,133,130,128,128,0",
        "13,13,141,141,5,5,5,133,133,130,129,129,128,128,0",
        "2,2,2,130,130,130,130,130,128,128,128,128,128,0,0",
        "134,134,141,5,5,5,5,133,133,133,130,128,128,128,0",
        "5,5,133,133,133,133,130,130,128,128,128,128,128,0,0",
        "142,142,4,4,4,4,132,132,132,132,130,128,128,128,0",
        "143,143,134,134,4,4,4,5,132,132,133,130,128,128,0"
    })[i]))
end

function fade_pal()
    fade=mid(fade,1)
    for i=0,15 do
        pal(i,ramp(fade_tbl[i+1],fade,1),1)
    end
end
-->8
--title screen state

state.title={}

function state.title:enter()
    --nothing
end

function state.title:update()
    if curr_state==state.title then
        if (btnf(❎) or btnf(🅾️)) switch_state(state.game,btnf(🅾️))
    end
end

function state.title:draw()
    cls()
    ctprint("pRESS ❎ TO START",64,61,7)
end
-->8
--game state

objects={
    play={},axe={},dust={},
    poof={},fire={},gc={},
    orb={},chan={},
   
    update_order={
        "play","axe","gc","orb",
        "chan","fire","dust","poof"
    },
    draw_order={
        "gc","chan","fire","axe",
        "orb","dust","poof","play"
    }
}

state.game={
    done=false,
    done_tmr=0,
    completed=false,
   
    --player
    play={
        --player create
        create=function(self,x,y)
            local obj={
                x=x,y=y,
                cx=1,cy=2,cw=6,ch=6,
                dx=0,dy=0,
                max_dx=8.25,max_dy=7.9,
                min_slide_dx=0.2,
                min_fall_dy=7.5,
                knockback_dx=8.25,
                acc=0.48,
                jump=5.5,
                gravity=0.4,
                friction=0.75,
                early_fall_factor=2.25,
                fly_factor=0.8,
                i_frame_tmr=0,
                stun_tmr=0,
                walking=false,
                jumping=false,
                falling=false,
                sliding=false,
                landed=true,
                holding_jump=false,
                stunned=false,
                has_fire=false,
                tunnel=false,
                boss=false,
               
                state="idle",
                frame=0,
                f_speed=0.6,
                anim={
                    idle={1},
                    slide={1},
                    walk=expand"2,3,4,5",
                    jump={6},
                    fall={7}
                },
                face⬅️=false,
               
                fire={
                    x=0,y=0, --temp values
                    cx=2,cy=5,cw=4,ch=3,
                },
               
                --player update
                update=function(self,objlist)
                    local fell_from_high=false
                   
                    --gravity and friction
                    self.dy+=self.gravity
                    self.dx*=self.friction
                   
                    --if i-frames
                    if self.i_frame_tmr>0 then
                        self.i_frame_tmr-=1
                    --if not i-frames
                    else
                        for gc in all(objects.gc) do
                            local px=self.x+self.cx+self.cw/2
                            local gx=gc.x+gc.cx+gc.cw/2
                            --if we hit the boss
                            if collide_sprites(self,gc) then
                                --oof
                                sfx(5,2)
                                --knock us back
                                self.dx+=self.knockback_dx*sgn(px-gx)
                                if (self.dy>0) self.dy-=self.jump*self.fly_factor
                                self.has_fire=false
                                self.stunned=true
                                self.walking=false
                                self.stun_tmr=44
                                self.i_frame_tmr=132
                                gc.hp_cutoff_error=rnd(gc.hp_cutoff_variance)
                                break
                            end
                        end
                    end
                   
                    for orb in all(objects.orb) do
                        --if we hit an orb
                        if collide_sprite_circle(self,orb) then
                            --and not i-frames
                            if self.i_frame_tmr<=0 then
                                --oof
                                sfx(5,2)
                                --make us fly up
                                self.dy-=self.jump*self.fly_factor
                                self.has_fire=false
                                self.stunned=true
                                self.walking=false
                                self.stun_tmr=44
                                self.i_frame_tmr=132
                                for gc in all(objects.gc) do
                                    gc.hp_cutoff_error=rnd(gc.hp_cutoff_variance)
                                end
                                break
                            end
                            orb:destroy(objects.orb)
                        end
                    end
                   
                    --if stunned
                    if self.stunned then
                        if self.stun_tmr>0 then
                            self.stun_tmr-=1
                        end
                    --if not stunned
                    else
                        --left+right
                        if btn(⬅️) and btn(➡️) then
                            self.face⬅️=true
                            self.walking=true
                            self.sliding=false
                            self.dx+=self.acc
                        --walk left
                        elseif btn(⬅️) then
                            self.face⬅️=true
                            self.walking=true
                            self.sliding=false
                            self.dx-=self.acc
                        --walk right
                        elseif btn(➡️) then
                            self.face⬅️=false
                            self.walking=true
                            self.sliding=false
                            self.dx+=self.acc
                        end
                       
                        --slide
                        if self.walking
                        and not (btn(⬅️) or btn(➡️))
                        and not self.falling
                        and not self.jumping
                        then
                            self.walking=false
                            self.sliding=true
                        end
                       
                        --jump
                        if (btnf(⬆️) or btnf(❎))
                        and self.landed
                        then
                            sfx(0,2)
                            self.dy-=self.jump
                            self.landed=false
                            self.holding_jump=true
                        end
                        --detect holding jump button
                        if not (btn(⬆️) or btn(❎)) then
                            self.holding_jump=false
                        end
                    end
                   
                    --check collision ⬆️ and ⬇️
                    --if moving down
                    if self.dy>0 then
                        self.falling=true
                        self.jumping=false
                       
                        self.dy=limit(self.dy,self.max_dy)
                        self.y+=self.dy
                        --if hitting floor
                        if collide_map(self,⬇️,0)
                        then
                            --if we're now landing
                            --and we're falling fast
                            if not self.landed
                            and self.dy>self.min_fall_dy
                            then
                                --oof
                                sfx(1,3)
                                fell_from_high=true
                            end
                            self.landed=true
                            self.falling=false
                            self.dy=0
                            --adjust
                            self.y-=(self.y+(self.cy+self.ch)+1)%8-1
                        else
                            self.landed=false
                        end
                    --if moving up
                    elseif self.dy<0 then
                        self.jumping=true
                       
                        --if not still holding jump
                        if not self.stunned
                        and not self.holding_jump
                        then
                            --increase gravity
                            self.dy+=self.gravity*self.early_fall_factor
                        end
                       
                        self.dy=limit(
                            self.dy,self.max_dy
                        )
                        self.y+=self.dy
                        --if hitting ceiling
                        if collide_map(self,⬆️,0)
                        then
                            self.dy=0
                            --adjust
                            self.y+=8-(self.y+self.cy-1)%8-1
                        end
                    end
                   
                    --check collision ⬅️ and ➡️
                    self.dx=limit(self.dx,self.max_dx)
                    --divide into intervals
                    local slice_w,abs_dx=self.cw,abs(self.dx)
                    while abs_dx>0 do
                        local slice=min(slice_w,abs_dx)
                        --if moving left
                        if self.dx<0 then
                            self.x-=slice
                            --if hitting left wall
                            if collide_map(self,⬅️,0)
                            then
                                --adjust
                                self.x+=slice
                                while not collide_map(self,⬅️,0) do
                                    self.x-=1
                                end
                                self.x+=8-(self.x+self.cx-1)%8-1
                                self.dx=0
                            end
                        --if moving right
                        elseif self.dx>0 then
                            self.x+=slice
                            --if hitting right wall
                            if collide_map(self,➡️,0)
                            then
                                --adjust
                                self.x-=slice
                                while not collide_map(self,➡️,0) do
                                    self.x+=1
                                end
                                self.x-=(self.x+(self.cx+self.cw)+1)%8-1
                                self.dx=0
                            end
                        end
                        abs_dx-=slice
                    end
                   
                    --if sliding too far
                    if self.sliding
                    and abs(self.dx)<self.min_slide_dx
                    then
                        --stop sliding
                        self.dx=0
                        self.sliding=false
                    end
                   
                    --handle falling from high
                    if fell_from_high then
                        state.game.dust:create(self.x,self.y)
                        self.has_fire=false
                    end
                   
                    --handle unstunning
                    if self.landed
                    and self.stun_tmr<=0
                    then
                        self.stunned=false
                    end
                   
                    --get all foreboding once
                    --you start walking
                    if not self.tunnel
                    and self.x+self.cx>=44
                    then
                        self.tunnel=true
                        music(0)
                    end
                   
                    --get all serious once
                    --you enter the boss room
                    if not self.boss
                    and self.x+self.cx>=856
                    then
                        self.boss=true
                        music(1)
                        --block off the exit
                        mset(105,25,10)
                        mset(105,26,10)
                        state.game.poof:create(844,204,true)
                        state.game.poof:create(844,212,true)
                        --there's a boss now
                        state.game.gc:create(1020-rnd(36),152,self)
                    end
                   
                    --grab fire
                    if not self.stunned
                    and collide_map(self,🅾️,1)
                    then
                        self.has_fire=true
                    end
                   
                    --throw weapons
                    if not self.stunned
                    and (btnf(🅾️) or btnf(⬇️))
                    then
                        sfx(2,3)
                        state.game.axe:create(self.x,self.y,self.dx,self.face⬅️)
                        self.has_fire=false
                    end
                   
                    --set animations
                    if self.jumping then
                        self.state="jump"
                    elseif self.falling then
                        self.state="fall"
                    elseif self.sliding then
                        self.state="slide"
                    elseif self.walking then
                        self.state="walk"
                    else
                        self.state="idle"
                    end
                   
                    self.frame=(self.frame+self.f_speed)%#self.anim[self.state]
                   
                    --set position of fire
                    if self.has_fire then
                        self.fire.x=self.x+(self.face⬅️ and -7 or 7)
                        self.fire.y=self.y-5
                    end
                end,
                --player draw
                draw=function(self)
                    --turn red if stunned
                    if (self.stunned) pal(15,4)
                   
                    if self.i_frame_tmr%11<6 then
                        spr(
                            self.anim[self.state][
                                flr(self.frame)+1
                            ],
                            self.x,self.y,
                            1,1,self.face⬅️
                        )
                    end
                    if self.has_fire then
                        line(
                            self.x+(
                                self.face⬅️ and 5 or 2
                            ),self.y+5,
                            self.x+(
                                self.face⬅️ and -3 or 10
                            ),self.y+3,
                            13
                        )
                        spr(
                            state.game.fire.anim[flr(time()/0.135)%3+1],
                            self.fire.x,self.fire.y
                        )
                    end
                   
                    pal()
                    palt(0,false)
                    palt(2,true)
                end,
                --player destroy
                destroy=destroy,
            }
            add(objects.play,obj)
        end
    },
   
    --axes
    axe={
        --axe create
        create=function(self,x,y,dx,aim⬅️)
            local obj={
                x=x,y=y,
                cx=3,cy=3,cw=2,ch=2,
                dx=(aim⬅️ and -1.7 or 1.7)+dx,dy=-2.8,
                max_dy=7.9,
                gravity=0.4,
                frame=0,
                f_speed=1,
                anim=expand"48,32,48,33,48,32,48,33",
                anim_flip=expand"0,0,2,0,3,3,1,3",
                aim⬅️=aim⬅️,
               
                --axe update
                update=function(self,objlist)
                    self.dy+=self.gravity
                    self.dy=limit(self.dy,self.max_dy)
                   
                    self.x+=self.dx
                    self.y+=self.dy
                   
                    self.frame=(self.frame+self.f_speed)%#self.anim
                   
                    --if we've hit a block
                    if collide_map(self,🅾️,0) then
                        --we don't exist anymore
                        self:destroy(objlist)
                    end
                end,
                --axe draw
                draw=function(self)
                    local flip_bits=self.anim_flip[flr(self.frame)+1]
                    spr(
                        self.anim[flr(self.frame)+1],
                        self.x,self.y,
                        1,1,
                        flip_bits&1!=(self.aim⬅️ and 1 or 0),
                        flip_bits&2>0
                    )
                end,
                --axe destroy
                destroy=destroy,
            }
            add(objects.axe,obj)
        end
    },
   
    --grinning colossus
    gc={
        --colossus create
        create=function(self,x,y,target)
            local obj={
                x=x,y=y,
                cx=2,cy=6,cw=44,ch=58,
                dx=0,
                acc=0.85,
                shoot_speed=1.25,
                target=target,
                target_epsilon=2,
                min_tx=896,max_tx=1191,
                min_ty=84,max_ty=223,
                state="rest",
                state_tmr=45,
                gen_state_tmr={
                    move=function(self)
                        return 75+rnd(50)
                    end,
                    charge=function(self)
                        if rnd()<0.75 then
                            return 40+rnd(30)
                        else
                            return rnd(8)
                        end
                    end,
                    shoot=function(self)
                        return 40+rnd(40)
                    end,
                    rest=function(self)
                        return 20+rnd(10)
                    end,
                    moth=function(self)
                        return 32767
                    end,
                    shock=function(self)
                        return 89
                    end
                },
               
                hp=100,
                hp_display=100,
                hp_speed=7.5,
                hp_heal=0.6,
                hp_nerf=4,
                hp_max_damage=7.5,
                hp_cutoff=75,
                hp_cutoff_variance=15,
                hp_cutoff_error=0,
               
                face=-1,
                mouth=false,
                eyes={
                    [-1]={
                        x1=6,y1=29,x2=6,y2=29,
                        px=-1,py=0,
                    },
                     [0]={
                        x1=13,y1=13,x2=47-13,y2=13,
                        px=0,py=-1,
                    },
                     [1]={
                        x1=47-6,y1=29,x2=47-6,y2=29,
                        px=1,py=0,
                    },
                },
               
                shock_target={
                    x=1040,y=96,
                    cx=3,cy=3,cw=2,ch=2,
                },
               
                --colossus shoot orbs
                shoot=function(self)
                    local eyes=self.eyes[self.face]
                    local ex1,ey1=self.x+eyes.x1+eyes.px,self.y+eyes.y1+eyes.py
                    local ex2,ey2=self.x+eyes.x2+eyes.px,self.y+eyes.y2+eyes.py
                   
                    local tx=self.target.x+self.target.cx+self.target.cw/2
                    local ty=self.target.y+self.target.cy+self.target.ch/2
                   
                    state.game.orb:create(ex1,ey1,tx,ty,self.shoot_speed,true)
                    state.game.orb:create(ex2,ey2,tx,ty,self.shoot_speed,true)
                end,
                --colossus burst
                burst=function(self)
                    sfx(6,1)
                    for i=1,20 do
                        self:explode()
                    end
                end,
                --colossus explode
                explode=function(self)
                    state.game.poof:create(
                        self.x+self.cx+rnd(self.cw),
                        self.y+self.cy+rnd(self.ch),
                        true
                    )
                end,
                --colossus target in bounds
                target_in_bounds=function(self)
                    local x1=self.target.x+self.target.cx
                    local x2=self.target.x+self.target.cx+self.target.cw-1
                    local y1=self.target.y+self.target.cy
                    local y2=self.target.y+self.target.cy+self.target.ch-1
                   
                    return round(x1)>self.min_tx
                    and round(x2)<self.max_tx
                    and round(y1)>self.min_ty
                    and round(y2)<self.max_ty
                end,
               
                --colossus update
                update=function(self,objlist)
                    local slx=self.x+self.cx
                    local srx=slx+self.cw-1
                    local tmx=self.target.x+self.target.cx+self.target.cw/2
                    local smx=self.x+self.cx+self.cw/2
                   
                    self.face=-1
                   
                    --if between colossus
                    if tmx>slx and tmx<srx
                    --and above eye level
                    and self.target.y+self.target.cy+self.target.ch-1<self.y+self.eyes[0].y1
                    then
                        --face forward
                        self.face=0
                    --if to left of colossus
                    elseif tmx<smx then
                        --face left
                        self.face=-1
                    --if to right of colossus
                    elseif tmx>smx then
                        --face right
                        self.face=1
                    end
                   
                    self.state_tmr-=1
                   
                    --set new states
                    --if done moving
                    roll_dice()
                    if self.state_tmr<=0 then
                        if self.state=="move" then
                            if dice<0.80 then
                                self.state="charge"
                            else
                                self.state="rest"
                            end
                        elseif self.state=="charge" then
                            self.state="shoot"
                        elseif self.state=="shoot" then
                            if dice<0.60 then
                                self.state="move"
                            elseif dice<0.80 then
                                self.state="charge"
                            else
                                self.state="rest"
                            end
                        elseif self.state=="rest" then
                            self.state="move"
                        elseif self.state=="moth" then
                            self.state="shock"
                        elseif self.state=="shock" then
                            self:burst()
                            state.game.done=true
                            state.game.done_tmr=100
                            self:destroy(objlist)
                        end
                       
                        self.state_tmr=self.gen_state_tmr[self.state](self)
                    end
                   
                    --if colossus is moving
                    if self.state=="move" or self.state=="moth" then
                        self.mouth=not self:target_in_bounds()
                        --if target out of bounds
                        if not self:target_in_bounds()
                        --or within epsilon
                        or abs(tmx-smx)<self.target_epsilon
                        then
                            --stop moving
                            self.dx=0
                        --if to left of colossus
                        elseif tmx<smx then
                            --move left
                            self.dx=-self.acc
                        --if to right of colossus
                        elseif tmx>smx then
                            --move right
                            self.dx=self.acc
                        end
                       
                        --chandelier crash
                        for chan in all(objects.chan) do
                            --if touching chandelier
                            if collide_sprites(self,chan) then
                                --destroy it
                                chan:destroy(objects.chan)
                                --we're shocked now
                                self.target=self.shock_target
                                self.state="shock"
                                self.state_tmr=self.gen_state_tmr[self.state](self)
                                self:burst()
                                --and we're dead
                                self.hp=0
                                --and we can't heal
                                self.hp_heal=0
                                --nor will weapons help
                                self.hp_cutoff=0
                            end
                        end
                       
                    --if colossus is charging
                    elseif self.state=="charge" then
                        self.mouth=true
                        self.dx=0
                    --if colossus is shooting
                    elseif self.state=="shoot" then
                        self.mouth=true
                        self.dx=0
                        --shoot every few frames
                        if self:target_in_bounds()
                        and self.state_tmr%4<1
                        then
                            self:shoot(objlist)
                        end
                    --if colossus is resting
                    elseif self.state=="rest" then
                        self.mouth=true
                        self.dx=0
                    --if colossus is shocked
                    elseif self.state=="shock" then
                        self.mouth=true
                        self.dx=0
                       
                        if self.state_tmr%10<1 then
                            self:explode()
                        end
                    end
                   
                    --move
                    self.x+=self.dx
                    --adjust for collision
                    if self.dx<0 then
                        if collide_map(self,⬅️,0) then
                            self.dx=0
                            self.x+=8-(self.x+self.cx-1)%8-1
                            if self.state=="move" then
                                self.mouth=true
                                roll_dice()
                                if dice<0.50 then
                                    self.state="charge"
                                else
                                    self.state="rest"
                                end
                                self.state_tmr=self.gen_state_tmr[self.state](self)
                            end
                        end
                    elseif self.dx>0 then
                        if collide_map(self,➡️,0) then
                            self.dx=0
                            self.x-=(self.x+(self.cx+self.cw)+1)%8-1
                            if self.state=="move" then
                                self.mouth=true
                                roll_dice()
                                if dice<0.50 then
                                    self.state="charge"
                                else
                                    self.state="rest"
                                end
                                self.state_tmr=self.gen_state_tmr[self.state](self)
                            end
                        end
                    end
                   
                    --handle hp
                    self.hp+=self.hp_heal
                    for axe in all(objects.axe) do
                        if collide_sprites(self,axe) then
                            sfx(5,2)
                            axe:destroy(objects.axe)
                            self.hp-=min(self.hp_max_damage*(0.5+rnd(0.5)),(self.hp-(self.hp_cutoff-self.hp_cutoff_error))/self.hp_nerf)
                        end
                    end
                   
                    self.hp=mid(0,self.hp,100)
                    self.hp_display+=(self.hp-self.hp_display)/self.hp_speed
                end,
                --colossus draw
                draw=function(self)
                    local x,y=self.x,self.y
                    if self.state=="shock" then
                        x+=2-rnd(4)
                        y+=rnd(4)
                    end
                   
                    --only render within
                    --the designated area
                    clip(
                        840-flr(state.game.cam.x),
                        -flr(state.game.cam.y),
                        408,216
                    )
                   
                    for i=0,63 do
                        local r=i<8 and round(8-sqrt(i*(16-i))) or i%56>=40 and 8 or 0
                        rectfill(x+r,y+i,x+47-r,y+i,0)
                    end
                   
                    --if facing left
                    if self.face<0 then
                        --draw left mouth
                        spr(
                            self.mouth and 26 or 24,
                            x,y+40,2,2,true
                        )
                        --fill gap
                        rectfill(x+40,y+40,x+47,y+55,0)
                    --if facing right
                    elseif self.face>0 then
                        --draw left mouth
                        spr(
                            self.mouth and 26 or 24,
                            x+32,y+40,2,2
                        )
                        --fill gap
                        rectfill(x,y+40,x+7,y+55,0)
                    --if facing forward
                    else
                        --draw left mouth
                        spr(
                            self.mouth and 30 or 28,
                            x+16,y+24,2,2
                        )
                        --fill gap
                        rectfill(x,y+40,x+47,y+55,0)
                    end
                   
                    local eyes=self.eyes[self.face]
                    local ex1,ey1=x+eyes.x1,y+eyes.y1
                    local ex2,ey2=x+eyes.x2,y+eyes.y2
                   
                    --draw left eye
                    if self.face<=0 then
                        circfill(ex1,ey1,4,7)
                        if (self.state=="shock") spr(0,ex1-3,ey1-4)
                        circfill(ex1+eyes.px,ey1+eyes.py,1,0)
                        pset(ex1+eyes.px,ey1+eyes.py,6)
                    end
                    --draw right eye
                    if self.face>=0 then
                        circfill(ex2,ey2,4,7)
                        if (self.state=="shock") spr(0,ex2-4,ey2-4,1,1,true)
                        circfill(ex2+eyes.px,ey2+eyes.py,1,0)
                        pset(ex2+eyes.px,ey2+eyes.py,6)
                    end
                   
                    clip()
                end,
                draw_hp=function(self)
                    rect(3,3,124,9,7)
                    rect(3,3,65,15)
                   
                    rectfill(4,4,123,8,0)
                    rectfill(4,4,64,14)
                   
                    if self.hp_display>=0.5 then
                        rectfill(5,5,flr(5.5+117*self.hp_display/100),7,4)
                    end
                   
                    tprint("gRINNING cOLOSSUS",8,9,7,13)
                end,
                --colossus destroy
                destroy=destroy,
            }
            add(objects.gc,obj)
        end
    },
   
    --orbs
    orb={
        --orb create
        create=function(self,x,y,tx,ty,speed,loud)
            if (loud) sfx(4,2)
           
            local angle=atan2(tx-x,ty-y)
            local obj={
                x=x,y=y,
                speed=speed,
                dx=speed*cos(angle),dy=speed*sin(angle),
                tx=tx,ty=ty,
                dist=100,
                r=2,
                timer=0,
                flash_speed=4,
               
                --orb update
                update=function(self,objlist)
                    self.timer=(self.timer+1)%self.flash_speed
                   
                    self.x+=self.dx
                    self.y+=self.dy
                    self.dist-=self.speed
                   
                    --if orb went far enough
                    if self.dist<=0
                    or self.y<80
                    then
                        state.game.poof:create(self.x,self.y,false)
                        self:destroy(objlist)
                    end
                   
                    local x=self.x-state.game.cam.x
                    local y=self.y-state.game.cam.y
                   
                    --if orb is past target
                    if self.x*sgn(self.dx)>=self.tx*sgn(self.dx)
                    and self.y*sgn(self.dy)>=self.ty*sgn(self.dy)
                    --and outside of screen
                    and (
                        x!=mid(-self.r-4,x,132+self.r)
                        or y!=mid(-self.r-4,y,132+self.r)
                    )
                    then
                        --don't process it
                        self:destroy(objlist)
                    end
                end,
                --orb draw
                draw=function(self)
                    local flash=self.timer<self.flash_speed/2
                    circfill(self.x,self.y,2,0)
                    circfill(self.x,self.y,1,flash and 9 or 7)
                    pset(self.x,self.y,flash and 7 or 9)
                end,
                --orb destroy
                destroy=destroy,
            }
            add(objects.orb,obj)
        end
    },
   
    --chandelier
    chan={
        --chandelier create
        create=function(self,x,y)
            local obj={
                x=x,y=y,
                cx=0,cy=0,cw=40,ch=3,
                dy=0,
                max_dy=3.45,
                gravity=0.075,
                hanging=true,
                burning=false,
                burn_tmr=0,
                burn_len=54,
               
                --chandelier rope
                rope={
                    x=x+18,y=y-48,
                    cx=0,cy=0,cw=4,ch=36
                },
               
                --chandelier update
                update=function(self,objlist)
                    --if we're not burning
                    if not self.burning
                    --and we're still hanging
                    and self.hanging
                    then
                        for play in all(objects.play) do
                            --if fire touching rope
                            if collide_sprites(self.rope,play.fire) then
                                --burn it
                                self.burning=true
                                self.burn_tmr=self.burn_len
                                music(-1)
                                state.game.completed=true
                                --get rid of the fire
                                play.has_fire=false
                                for gc in all(objects.gc) do
                                    gc.target=self
                                    gc.state="moth"
                                    gc.state_tmr=gc.gen_state_tmr[gc.state](gc)
                                end
                            end
                        end
                    end
                   
                    --if we are burning
                    if self.burning then
                        --and it's still burning
                        if self.burn_tmr>0 then
                            --burn it some more
                            self.burn_tmr-=1
                            --also make it explode
                            if self.burn_tmr%5==0 then
                                state.game.poof:create(self.rope.x+2,self.rope.y+36-(self.burn_tmr/self.burn_len*36),true)
                            end
                            --if we're done burning
                            if self.burn_tmr<=0 then
                                self.burning=false
                                self.hanging=false
                            end
                        end
                    end
                   
                    --if we aren't hanging
                    if not self.hanging then
                        self.dy+=self.gravity
                        self.dy=limit(self.dy,self.max_dy)
                        self.y+=self.dy
                       
                        --failsafe if too low
                        if self.y>=256 then
                            for gc in all(objects.gc) do
                                gc.target=gc.shock_target
                                gc.state="shock"
                                gc.state_tmr=gc.gen_state_tmr[gc.state](gc)
                                gc:burst()
                            end
                            self:destroy(objlist)
                        end
                    end
                end,
                --chandelier draw
                draw=function(self)
                    if self.hanging then
                        rectfill(self.rope.x,self.rope.y,self.rope.x+3,self.rope.y+48,
                            self.burning
                            and ramp(
                                expand"0,0,3,8",
                                self.burn_tmr,
                                self.burn_len
                            ) or 8
                        )
                        rectfill(self.rope.x+1,self.rope.y+1,self.rope.x+2,self.rope.y+34,
                            self.burning
                            and ramp(
                                expand"0,0,3,8,12",
                                self.burn_tmr,
                                self.burn_len
                            ) or 12
                        )
                        rectfill(self.rope.x+1,self.rope.y+36,self.rope.x+2,self.rope.y+47,
                            self.burning
                            and ramp(
                                expand"0,0,3,8,12,6",
                                self.burn_tmr,
                                self.burn_len
                            ) or 6
                        )
                    end
                   
                    for i=0,2 do
                        sspr(
                            8+20*i,24,20,8,
                            self.x,self.y+8*i
                        )
                        sspr(
                            8+20*i,24,20,8,
                            self.x+40,self.y+8*i,-20,8
                        )
                    end
                end,
                --chandelier destroy
                destroy=destroy,
            }
            add(objects.chan,obj)
        end
    },
   
    --dust cloud
    dust={
        --dust cloud create
        create=function(self,x,y)
            local obj={
                x=x,y=y,
                frame=0,
                f_speed=0.35,
                anim=expand"0,19,20,21,22,23",
               
                --dust cloud update
                update=function(self,objlist)
                    self.frame+=self.f_speed
                    if self.frame>=#self.anim then
                        self:destroy(objlist)
                    end
                end,
                --dust cloud draw
                draw=function(self)
                    local sp=self.anim[flr(self.frame)+1]
                    if sp!=0 then
                        spr(sp,self.x-8,self.y)
                        spr(sp,self.x+8,self.y,1,1,true)
                    end
                end,
                --dust cloud destroy
                destroy=destroy,
            }
            add(objects.dust,obj)
        end
    },
   
    --poof
    poof={
        --poof create
        create=function(self,x,y,loud)
            if (loud) sfx(3,3)
           
            local obj={
                x=x,y=y,
                anim=expand"34,35,36,37,38,39",
                frame=0,
                f_speed=.5,
               
                --poof update
                update=function(self,objlist)
                    self.frame+=self.f_speed
                    if self.frame>=#self.anim then
                        self:destroy(objlist)
                    end
                end,
                --poof draw
                draw=function(self)
                    spr(
                        self.anim[flr(self.frame)+1],
                        self.x-4,self.y-4
                    )
                end,
                --poof destroy
                destroy=destroy,
            }
            add(objects.poof,obj)
        end
    },
   
    --fire on the wall
    fire={
        anim=expand"16,17,18",
       
        --fire create
        create=function(self,x,y)
            local obj={
                x=x,y=y,
                anim=self.anim,
                frame=0,
                f_speed=0.15,
               
                --fire update
                update=function(self,objlist)
                    self.frame=(self.frame+self.f_speed)%#self.anim
                end,
                --fire draw
                draw=function(self)
                    spr(
                        self.anim[flr(self.frame)+1],
                        self.x,self.y
                    )
                end,
                --fire destroy
                destroy=destroy,
            }
            add(objects.fire,obj)
        end
    },
   
    --camera
    cam={
        x=0,y=0,--temp vals
        min_x=0,min_y=0,
        max_x=1128,max_y=112,
        max_dx=10,max_dy=10,
        speed=5,
       
        init=function(self)
            local play=objects.play[1]
            local tx,ty=play.x-60,play.y-60
           
            self.x=mid(self.min_x,tx,self.max_x)
            self.y=mid(self.min_y,ty,self.max_y)
        end,
        update=function(self)
            local play=objects.play[1]
            local tx,ty=play.x-60,play.y-60
           
            self.x+=limit((tx-self.x)/self.speed,self.max_dx)
            self.y+=limit((ty-self.y)/self.speed,self.max_dy)
            self.x=mid(self.min_x,self.x,self.max_x)
            self.y=mid(self.min_y,self.y,self.max_y)
        end
    }
}

function state.game:enter(prev,timer)
    self.timer_active=timer
   
    if (self.timer_active) self.timer={m=0,s=0,f=0}
   
    --get rid of all objects
    --just in case
    for class,objs in pairs(objects) do
        if class!="update_order"
        and class!="draw_order"
        then
            objects[class]={}
        end
    end
   
    --place fire on the wall
    for x=107,153 do
        for y=9,19 do
            if fget(mget(x&127,y+((x&-128)>>2)),1) then
                self.fire:create(x*8,y*8-6)
            end
        end
    end
   
    --spawn chandelier
    self.chan:create(1024,96)
   
    --spawn player
    self.play:create(20,32)
   
    --initialize camera values
    self.cam:init()
   
    --we're not done with the game
    reload(0x1000,0x1000,0x2000)
    self.done=false
    self.completed=false
end

function state.game:update()
    --update everything
    for class in all(objects.update_order) do
        for obj in all(objects[class]) do
            obj:update(objects[class])
        end
    end
   
    --tick timer
    if self.timer_active
    and not self.completed
    and self.timer.m<60
    then
        self.timer.f+=1
        if self.timer.f>=30 then
            self.timer.s+=self.timer.f\30
            self.timer.f%=30
            if self.timer.s>=60 then
                self.timer.m+=self.timer.s\60
                self.timer.s%=60
                if self.timer.m>=60 then
                    self.timer.m=60
                    self.timer.s=0
                    self.timer.f=0
                end
            end
        end
    end
   
    --if we're done
    if self.done then
        --count down to the credits
        if self.done_tmr<=0 then
            if (curr_state==state.game) switch_state(state.credits)
        else
            self.done_tmr-=1
        end
    end
end

function state.game:draw()
    cls()
   
    --set cam position
    self.cam:update()
    camera(self.cam.x,self.cam.y)
   
    palt(0,false)
    palt(2,true)
   
    --draw map
    map(0,0,0,0,128,32)
    map(0,32,1024,0,128,32)
   
    --draw everything
    for class in all(objects.draw_order) do
        for obj in all(objects[class]) do
            obj:draw()
        end
    end
   
    --draw instructions
    bprint("yOU hAVE tO",8,148,7,13)
    bprint("bURN tHE rOPE",8,156,7,13)
    tprint("A mAZAPAN GAME\nPORTED TO pico-8\nBY jOSIAH wINSLOW",8,166,7,13)
    tprint("mUSIC BY hENRIK nAMARK\nREACHGROUND.SE\n\naRRANGED BY jOSIAH wINSLOW",68,148,7,13)
    tprint("tHANKS TO cHRISTIAN dRYDEN,\nuMAMI, gb & kISTA gROSSEN",68,178,7,13)
    tprint("1. tHERE'S A BOSS AT\nTHE END OF THIS TUNNEL",273,98,7,13)
    tprint("2. yOU CAN'T HURT\nHIM WITH YOUR\nWEAPONS",398,120,7,13)
    tprint("3. tO KILL HIM YOU HAVE TO\nBURN THE ROPE ABOVE",534,152,7,13)
    tprint("hAVE FUN!",644,200,7,13)
   
    camera()
   
    --draw colossus hp
    --in a stationary place
    for gc in all(objects.gc) do
        gc:draw_hp()
    end
   
    if self.timer_active then
        rectfill(5,20,39,28,3)
        rectfill(4,19,38,27,13)
        rectfill(3,18,37,26,7)
        rectfill(4,19,36,25,0)
        print(format_timer(self.timer),5,20,7)
    end
   
    palt()
end
-->8
--credits state

state.credits={
    prev_state=nil,
    prev_fading=true,
   
    fade_speed=1/58,
   
    scene=0,
    scenes={
        {
            end_note=201,
            draw=function(this)
                --nothing
            end,
        },
        {
            end_note=235,
            draw=function(this)
                cprint("yOU hAVE tO bURN tHE rOPE",64,56,7)
                ctprint("tHANK YOU FOR PLAYING!",64,66,7)
            end,
        },
        {
            end_note=273,
            draw=function(this)
                this:draw_scene(1,2,38)
                cprint("dESIGN, cODE,",90,50,7)
                cprint("gRAPHICS",90,56,7)
                ctprint("kIAN bASHIRI",90,65,7)
                ctprint("(MAZAPAN.SE)",90,71,7)
            end,
        },
        {
            end_note=311+3,
            draw=function(this)
                this:draw_scene(2,74,38)
                cprint("mUSIC",38,53,7)
                ctprint("hENRIK nAMARK",38,62,7)
                ctprint("(REACHGROUND.SE)",38,68,7)
            end,
        },
        {
            end_note=349+7,
            draw=function(this)
                this:draw_scene(3,2,38)
                cprint("aDDITIONAL dESIGN",90,53,7)
                ctprint("hENRIK nAMARK",90,62,7)
                ctprint("cHRISTIAN dRYDEN",90,68,7)
            end,
        },
        {
            end_note=387+11,
            draw=function(this)
                this:draw_scene(4,74,38)
                cprint("sPECIAL THANKS TO",38,42,7)
                ctprint("uMAMI",38,51,7)
                cprint("eXTRA SPECIAL",38,64,7)
                cprint("THANKS TO",38,70,7)
                ctprint("kIAN bASHIRI )",38,79,7)
            end,
        },
        {
            end_note=767,
            draw=function(this)
                --nothing
            end,
        },
        {
            end_note=32767,
            draw=function(this)
                if state.game.timer_active then
                    cprint("gAME oVER",64,48,7)
                    ctprint("tIME: "..format_timer(state.game.timer),64,57,7)
                    ctprint("pRESS ❎ TO PLAY AGAIN",64,67,7)
                    ctprint("pRESS 🅾️ TO TIME YOURSELF",64,73,7)
                else
                    cprint("gAME oVER",64,53,7)
                    ctprint("pRESS ❎ TO PLAY AGAIN",64,63,7)
                    ctprint("pRESS 🅾️ TO TIME YOURSELF",64,69,7)
                end
            end,
        },
    },
    scene_fading=false,
   
    prev_note=0,
    lyric=1,
    lyrics={
        {
            words="cONGRATULATIONS",
            range=expand"228,237",
        },
        {
            words="yOU MANAGED TO KILL",
            range=expand"243,248",
        },
        {
            words="THAT BOSS, YOU SEE?",
            range=expand"249,255",
        },
        {
            words="tHE gRINNING cOLOSSUS",
            range=expand"259,268",
        },
        {
            words="yOU'RE THE HERO WE ALL",
            range=expand"274,280",
        },
        {
            words="WISH WE COULD BE",
            range=expand"281,287",
        },
        {
            words="yOU MADE IT THROUGH THE TUNNEL",
            range=expand"291,300",
        },
        {
            words="tHEN YOU GRABBED THAT FIRE",
            range=expand"306,314",
        },
        {
            words="ON THE WALL",
            range=expand"315,320",
        },
        {
            words="yOU JUMPED UP ABOVE HIM",
            range=expand"323,332",
        },
        {
            words="tHEN YOU BURNED THE ROPE",
            range=expand"338,344",
        },
        {
            words="AND SAVED US ALL",
            range=expand"345,352",
        },
        {
            words="yOU BURNED THE ROPE",
            range=expand"371,376",
        },
        {
            words="AND SAVED US ALL",
            range=expand"377,384",
        },
        {
            words="yOU BURNED THE ROPE",
            range=expand"403,408",
        },
        {
            words="AND SAVED US",
            range=expand"409,415",
        },
        {
            words="nOW YOU'RE A HERO",
            range=expand"420,426",
        },
        {
            words="yOU MANAGED TO",
            range=expand"427,431",
        },
        {
            words="BEAT THE WHOLE DAMN GA-AME",
            range=expand"432,444",
        },
        {
            words="wE'RE HAPPY YOU MADE IT",
            range=expand"451,456",
        },
        {
            words="bUT HOW ARE YOU GONNA SPEND",
            range=expand"459,466",
        },
        {
            words="THE REST OF THIS DA-AY",
            range=expand"467,476",
        },
        {
            words="mAYBE WATCH A VIDEO",
            range=expand"484,499",
        },
        {
            words="mAYBE PRESS REFRESH",
            range=expand"500,508",
        },
        {
            words="AND START AGAIN",
            range=expand"509,513",
        },
        {
            words="nOW YOU'RE A HERO",
            range=expand"516,522",
        },
        {
            words="yOU MANAGED TO",
            range=expand"523,527",
        },
        {
            words="BEAT THE WHOLE DAMN GA-AME",
            range=expand"528,540",
        },
        {
            words="wE'RE HAPPY YOU MADE IT",
            range=expand"547,552",
        },
        {
            words="bUT HOW ARE YOU",
            range=expand"555,559",
        },
        {
            words="GONNA SPEND THE REST OF THIS DA-AY",
            range=expand"560,572",
        },
        {
            words="mAYBE WATCH A VIDEO",
            range=expand"580,595",
        },
        {
            words="mAYBE PRESS REFRESH",
            range=expand"596,604",
        },
        {
            words="AND START AGAIN",
            range=expand"605,609",
        },
        {
            words="yES IT'S OVER NOW",
            range=expand"612,621",
        },
        {
            words="WE DIDN'T WANT TO",
            range=expand"627,634",
        },
        {
            words="MAKE A LONGER GAME",
            range=expand"635,643",
        },
        {
            words="tHIS IS IT",
            range=expand"644,648",
        },
        {
            words="i SWEAR IT'S TRUE",
            range=expand"649,656",
        },
        {
            words="OOHH OHH",
            range=expand"661,665",
        },
        {
            words="nOW YOU'RE A HERO",
            range=expand"676,682",
        },
        {
            words="YOU MANAGED TO",
            range=expand"683,687",
        },
        {
            words="BEAT THE WHOLE DAMN GA-AME",
            range=expand"688,700",
        },
        {
            words="wE'RE HAPPY YOU MADE IT",
            range=expand"707,714",
        },
        {
            words="BUT HOW ARE YOU",
            range=expand"715,719",
        },
        {
            words="GONNA SPEND THE REST OF THIS DA-AY",
            range=expand"720,732",
        },
        {
            words="mAYBE WATCH A VIDEO",
            range=expand"740,755",
        },
        {
            words="mAYBE PRESS REFRESH AND START AGAIN",
            range=expand"756,771",
        },
    },
}

function state.credits:enter(prev)
    self.prev_state=prev
    self.prev_note=0
    self.scene=0
    self.lyric=1
    self.prev_fading=true
    music(6)
end

function state.credits:update()
    --fade out the previous state
    if self.prev_fading then
        self.prev_state:update()
        fade+=self.fade_speed
        if fade>=1 then
            self.prev_fading=false
            fade=1
            self.scene+=1
        end
    end
   
    local note=stat(24)>=0 and stat(24)*32+stat(20) or -1
    local lyric=self.lyrics[self.lyric]
    local scene=self.scenes[self.scene]
   
    --try fading back in
    if (not self.prev_fading and scene and not self.scene_fading) fade=max(fade-self.fade_speed)
   
    --if we've faded back in
    if fade==0
    --and we're on the last scene
    and self.scene==#self.scenes
    then
        if (btnf(❎) or btnf(🅾️)) switch_state(state.game,btnf(🅾️))
    end
   
    --failsafe for when music sync
    --goes horribly wrong
    if note>0 and note<self.prev_note then
        self.lyric=1
        while self.lyric<=#self.lyrics and note>self.lyrics[self.lyric].range[2] do
            self.lyric+=1
        end
        lyric=self.lyrics[self.lyric]
       
        self.scene=1
        while self.scene<=#self.scenes and note>self.scenes[self.scene].end_note do
            self.scene+=1
        end
        scene=self.scenes[self.scene]
        self.scene_fading=false
    end
   
    --do this if we don't think
    --something's gone wrong
    if note-self.prev_note!=32 then
        --advance the lyric and scene
        --if we need to
        if (lyric and note>lyric.range[2]) self.lyric+=1
        if (scene and note>scene.end_note) self.scene_fading=true
       
        --remember the last note
        self.prev_note=note
    end
   
    --advance the scene when
    --it's faded out
    if self.scene_fading then
        fade+=self.fade_speed
        if fade>=1 then
            fade=1
            self.scene_fading=false
            self.scene+=1
        end
    end
end

function state.credits:draw()
    cls()
    if (self.prev_fading) self.prev_state:draw()
   
    local note=stat(24)>=0 and stat(24)*32+stat(20) or -1
    local lyric=self.lyrics[self.lyric]
    local scene=self.scenes[self.scene]
   
    if (scene) scene.draw(self)
    if (lyric and note==mid(lyric.range[1],note,lyric.range[2])) ctprint(lyric.words,64,note>=416 and 61 or 120,2)
end

--draw rle-encoded pics
function state.credits:draw_scene(index,x,y)
    local scene_px=expand"4096,4529,4988,5915"
   
    local px=scene_px[index]
    local px_end=px+ssget(px,3)+2
    local xx,yy=0,0
   
    px+=3
    while px<px_end do
        local c=ssget(px,2)
        px+=2
        if c<0x80 then
            while c>=0 do
                local lim=min(51,xx+c)
                rectfill(x+xx,y+yy,x+lim,y+yy,ssget(px))
                c-=lim+1-xx
                xx=lim+1
                if (xx>51) xx=0 yy+=1
            end
            px+=1
        else
            for i=0x80,c do
                pset(x+xx,y+yy,ssget(px))
                xx+=1
                if (xx>51) xx=0 yy+=1
                px+=1
            end
        end
    end
end
