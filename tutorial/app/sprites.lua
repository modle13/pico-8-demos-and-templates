function create_stationary(sprite_ids, x_start, y_start, x_offset, y_offset, collection, name)
    -- count is used to calculate vertical offset of sprites
    count = 1
    for i, v in pairs(sprite_ids) do
        x = x_start + count * x_offset
        if (v == 49) x += 1
        y = y_start + count * y_offset
        actor = create_actor_in_collection(v, x, y, collection, name)
        count = count + 1
    end
end

function create_actor_in_collection(sprite_id, x, y, collection, name)
    actor = actor:new(sprite_id, x, y, name)
    if contains(no_animation_sprite_names, name) then
        actor.frames = 1
    end
    add(collection, actor)
    return actor
end
