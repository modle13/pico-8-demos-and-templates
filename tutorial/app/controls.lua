function handle_button_presses()
    if btn(5) then
        if (btnp(2) or btnp(3)) cycle_cursor_type(btnp(2)) return
        if (state.movement_mode=='cursor' and (btnp(0) or btnp(1))) cycle_selection_sprite(cursor1.name,btnp(0)) return
    end
    if btnp(4) then
        if state.movement_mode=='cursor' then cursor1:handle_target() else player1:handle_player_target() end
    end
end
