function toggle_game_modes(mode)
    if mode=='tutorial' then
        -- state.tutorial_enabled=not state.tutorial_enabled
        if not state.tutorial_enabled then
            clean_up_tutorial_sprites()
        end
    end
    manage_tutorial_entry()
    menuitem(4,tutorial_str,function() toggle_game_modes("tutorial") end)
end

function manage_tutorial_entry()
    if (state.tutorial_enabled) then tutorial_str="exit tutorial" else tutorial_str="open tutorial" end
end

manage_tutorial_entry()
menuitem(2,"toggle counters",function() state.show_counts=not state.show_counts end)
-- menuitem(3, "toggle coords",   function() state.show_coords = not state.show_coords end)
menuitem(4,tutorial_str,function() toggle_game_modes("tutorial") end)
