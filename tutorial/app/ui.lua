function draw_ui_sprite(a, name)
    x = a.x
    y = a.y
    local sx = x * globals.cell_size
    local sy = y * globals.cell_size
    spr(a.sprite_id + a.frame, sx, sy, 1, 1, a.direction_x==-1, a.direction_y==-1)
end

function draw_dialog()
    -- can params be split/a table?
    map(97,28,8,96,14,3)
    print('you are me... what is this...',12,100)
end
