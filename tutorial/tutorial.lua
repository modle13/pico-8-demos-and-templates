-- title: conveyor belt simulator
-- author: modle
-- description:
--     this is prototype/reference code
--     beware inefficiencies/inconsistencies


-----------
-- CORE
-----------

function _init()
    -- set up room
    cam1 = cam:new()
    set_room(0, 0)

    -- create sprites
    create_counters()
    create_cursors()

    -- make player
    player1 = player:new()
    player_dummy = add_actor(60, 10, 8, 'player')
    temp_player_x, temp_player_y = player1.x, player1.y

    state.reference_belt = instantiate_belt(20000, 20000)

    -- start music
    -- music(2)
    get_wave_cells()

    -- make_building(23, 11.5, 3.5)
    -- make_building(1, 6.5, 3.5)

    -- export_map_sprites()
end

function _update()
    if (state.tutorial_enabled) return
    handle_button_presses()
    if state.movement_mode == 'cursor' then
        cursor1:control()
    else
        player1:control()
    end

    foreach(state.products, move_actor)

    foreach(state.production_structures, update_building)

    foreach(state.room_map_sprites, animate_stationary)

    update_reference_sprites()
    foreach(state.wave_cells, update_map_cells)
    manage_frame()
    update_product_pos_map()
end

function _draw()
    -- clear screen
    cls()

    if state.tutorial_enabled then
        map(112,0,0,0,16,16)
        draw_tutorial()
    else
        map(0,0,0,0,16,16)
        map(globals.structure_map_x,0,0,0,16,16)

        handle_coordinates_debug()
        foreach(state.visible_sprites,draw_actor)
        player1:draw()

        draw_counters()
        draw_ui_sprite(cursor1, 'cursor')
    end

    if state.dialog_mode then
        draw_dialog()
    end

    cam1:update()
end
