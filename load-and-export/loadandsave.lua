MAP_WIDTH = 16
MAP_HEIGHT = 16
cell_size = 8
room_x = 0
room_y = 0

msg='drop a text file here\nto load data'
mydata=''

function _init()
end

function _update()
    -- reads from file drop
    if stat(120) then
        printh('found stat data')
        serial(0x800,0x4300,4864)
        s=""
        for i=0,4864 do
            s..=chr(@(0x4300+i))
        end
        mydata=s
    end
end

function _draw()
    -- clear screen
    cls()

    map()

    --foreach(actor, draw_actor)
    draw_actor()
    handle_camera()

end

function draw_actor()
    -- local sx = (a.x * cell_size - cell_size / 2)
    -- local sy = (a.y * cell_size - cell_size / 2)
    spr(1, 5, 5)
    print(msg, 5, 20)
    print(mydata, 5, 40)
    -- writes to clipboard
    --printh(mydata,"@clip",true)
end

function handle_camera()
    camera(room_x * MAP_WIDTH * cell_size, room_y * MAP_HEIGHT * cell_size)
end
