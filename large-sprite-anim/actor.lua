function draw_actor(a) a:draw() end
function move_actor(a) a:move() end

actor={}

function actor:new(sprite_id,x,y,name,frm_inc,frms)
    local a={
        sprite_id=tonum(sprite_id),
        x=x,
        y=y,
        name=name,
        label=name,
        dx=0,
        dy=0,
        frm=0,
        frms=frms or 2,
        friction=1,
        w=0.4,
        h=0.4,
        frm_inc=frm_inc,
        tm=time(),
    }
    make_obj(a,self)
    return a
end

function actor:move()
    if next_is_solid(self,self.dx,0) then
        self.dx*=-0.1
    end
    if next_is_solid(self,0,self.dy) then
        self.dy*=-0.1
    end

    self.x+=self.dx
    self.y+=self.dy

    self.frm+=abs(self.dx)
    self.frm+=abs(self.dy)

    self.frm+=self.frm_inc or 0.1
    self.frm%=self.frms

    if self.friction!=0 then
        self.dx*=(0.65)
        self.dy*=(0.65)
    end

    if abs(self.dx)<0.001 and abs(self.dy)<0.001 then
        self.dx,self.dy=0,0
    end
end

function actor:draw()
    local sx,sy=(self.x*8-4),(self.y*8-4)
    spr(self.sprs[1]+self.frm,sx,sy,1,1,self.flip_x or false)
    spr(self.sprs[2]+self.frm,sx+8,sy,1,1,self.flip_x or false)
end
